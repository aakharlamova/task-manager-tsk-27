package ru.kharlamova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.api.IBusinessService;
import ru.kharlamova.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    @NotNull
    Project add(@Nullable String userId, @Nullable String name, @Nullable String description);

}
