package ru.kharlamova.tm.dto;

import lombok.Getter;
import lombok.Setter;
import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class Domain implements Serializable {

    private List<Project> projects;

    private List<Task> tasks;

    private List<User> users;

}