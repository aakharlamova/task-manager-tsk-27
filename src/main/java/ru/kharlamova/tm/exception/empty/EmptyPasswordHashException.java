package ru.kharlamova.tm.exception.empty;

import ru.kharlamova.tm.exception.AbstractException;

public class EmptyPasswordHashException extends AbstractException {

    public EmptyPasswordHashException() {
        super("Error! Password hash is empty.");
    }

}
