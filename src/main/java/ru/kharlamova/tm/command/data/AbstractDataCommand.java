package ru.kharlamova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kharlamova.tm.command.AbstractCommand;
import ru.kharlamova.tm.dto.Domain;
import ru.kharlamova.tm.exception.entity.ObjectNotFoundException;

public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";
    protected static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        if (serviceLocator == null) throw new ObjectNotFoundException();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new ObjectNotFoundException();
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
    }

}
